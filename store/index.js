export const state = () => ({
    token: '',
    todos: [],
})

export const mutations = {
    setToken(state, data) {
        state.token = data;
    },
    setTodos(state, data) {
        state.todos = data;
    }
}

export const actions = {
    async createTodo ({ commit, state }, params) {
        const todos = await this.$axios.$post(`${process.env.api}/todo/todos`, params, {
            headers: {
                Authorization: `Bearer ${state.token}`
            }
        })
    },
    async getTodoList ({commit, state}) {
        const todoList = await this.$axios.$get(`${process.env.api}/todo/todos`, {
            headers: {
                Authorization: `Bearer ${state.token}`
            }
        })
        commit('setTodos', todoList)
    },
    async deleteTodo ({commit, state}, id) {
        await this.$axios.$delete(`${process.env.api}/todo/todos/${id}`, {
            headers: {
                Authorization: `Bearer ${state.token}`
            }
        })
    },
    async editTodo ({commit, state}, param){
        console.log(param);
        await this.$axios.$put(`${process.env.api}/todo/todos/${param.id}`, param, {
          headers: {
            Authorization: `Bearer ${state.token}`,
          }
        })
      },
}

export const getters = {
    getToken (state) {
        return state.token
    },
    getTodoList (state) {
        return state.todos
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}